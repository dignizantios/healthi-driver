//
//  Basicstuff.swift
//  Healthi Driver
//
//  Created by YASH on 26/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON
import Alamofire

struct GlobalVariables {
    
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    //MARK:- API flag
    
    static var localTimeZoneName: String { return TimeZone.current.identifier }
    static let deviceType = "1"
    static let strLang = "0"
    static let strSuccessResponse = "1"
    static let strAccessDenied = "-1"
    static let deviceToken = ""
    static let registerID = "1212"
    //MARK: - PhoneLength
    
    static let phoneNumberLimit = 10
    
}


let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard


//MARK: - Setup mapping
let mapping:StringMapping = StringMapping.shared()
let Arabic = "ar"
let English = "en"
var isEnglish = true
var lang = "0"

func setLangauge(language:String)
{
    
    if language == Arabic
    {
        isEnglish = false
        Defaults.set(Arabic, forKey: "language")
        Defaults.set(1, forKey: "lang")
        lang = "1"
        Defaults.synchronize()
    }
    else
    {
        isEnglish = true
        Defaults.set(English, forKey: "language")
        Defaults.set(0, forKey: "lang")
        lang = "0"
        Defaults.synchronize()
    }
    print("Language - ",Defaults.value(forKey: "language"));
    
    StringMapping.shared().setLanguage()
}

func getCommonString(key:String) -> String
{
    return mapping.string(forKey: key) ?? ""
}


//MARK: - Storagae


func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}


//MARK: - Set Toaster

func makeToast(strMessage : String){
    /*
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    */
}


extension UIViewController : NVActivityIndicatorViewable
{
    
    
    
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - Alert
    
    func setAlert(msg:String)
    {
        let alert = UIAlertView()
        alert.title = "Liber"
        alert.message = msg
        alert.addButton(withTitle:"Ok")
        alert.show()
    }
    
    
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    
    // MARK: -  For Loader NVActivityIndicatorView Process
    
    func showLoader()
    {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    
    //MARK: - Navigation Controller Setup
    
    func setupNavigationbar(title:String)
    {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeGreenColor
        self.navigationItem.hidesBackButton = true
        
        let bounds = self.navigationController!.navigationBar.bounds
   //     self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        
        if let vwnav = Bundle.main.loadNibNamed("ViewNav", owner: nil, options: nil)?[0] as? ViewNav
        {
            
            var hightOfView = 0
            
            if UIScreen.main.bounds.height >= 812
            {
                hightOfView = 44
            }
            else
            {
                hightOfView = 20
            }
            
            vwnav.frame = CGRect(x: 0, y: 0, width: self.navigationController?.navigationBar.frame.width ?? 320, height: vwnav.frame.height + CGFloat(hightOfView))
            vwnav.lblTitle.text = title
            self.navigationController?.view.addSubview(vwnav)
        }
        
        /*
        let leftButton = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
        leftButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.font : themeFont(size: 35, fontname: .medium)], for: .normal)
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        */
        
        /*
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
         */
        
    }
    
    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupNavigationbarwithBackButton(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        self.removeNavigationCustomView()
        
        let leftButton = UIButton(type: .system)
        
        
        leftButton.tintColor = UIColor.white
        leftButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        if !isEnglish
        {
            leftButton.setTitle(" \(getCommonString(key: "Back_key"))", for: .normal)
            leftButton.setImage(UIImage(named: "ic_arrow-left"), for: .normal)
            leftButton.transform = CGAffineTransform(scaleX: -1, y: 1)
            leftButton.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
            leftButton.sizeToFit()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: leftButton)
        }else{
            leftButton.setImage(UIImage(named: "ic_arrow-left"), for: .normal)
            leftButton.setTitle(" \(getCommonString(key: "Back_key"))", for: .normal)
            leftButton.sizeToFit()
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftButton)
        }
 
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 18, fontname: .medium)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeGreenColor
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    func setNavigationBarTransparent()
    {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
        
        self.removeNavigationCustomView()
    }
    
    
    func removeNavigationCustomView()
    {
        var yValue : CGFloat = 20
        
        if UIScreen.main.bounds.height >= 812
        {
            yValue = 44
        }
        else
        {
            yValue = 20
        }
        
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: yValue, width: self.view.bounds.width, height: 0)
        
        if let viewWithTag = self.navigationController?.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
            
        }
    }
    
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeGreenColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = OrignalFormatter
        guard let convertedDate = dateformatter.date(from: strDate) else {
            return ""
        }
        dateformatter.dateFormat = YouWantFormatter
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
    
    //MARK: - Logout API
    
    /*
    func logoutAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(userURL)\(urlLogout)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "user_id" : getUserDetail("user_id"),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "1212",
                         ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        Defaults.removeObject(forKey: "userDetails")
                        Defaults.removeObject(forKey: "rideAlreadyStared")
                        Defaults.removeObject(forKey: "CurrentDriverID")
                        
                        let mainViewController = self.sideMenuController!
                        
                        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                        
                        let rearNavigation = UINavigationController(rootViewController: vc)
                        AppDelegate.shared.window?.rootViewController = rearNavigation
                        
                        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    */
    
}

