//
//  UImageSelection + Extension.swift
//  Liber
//
//  Created by YASH on 24/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController
{
    
    //MARK: - Image Choose Methods
    func ShowChooseImageOptions(picker : UIImagePickerController)
    {
        let alertController = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            print("camera button tapped")
            self.openCamera(picker : picker)
        })
        
        let  galleryButton = UIAlertAction(title: "Photos", style: .default, handler: { (action) -> Void in
            
            print("Photos button tapped")
            self.openGallary(picker : picker)
        })
        let  dismissButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(dismissButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func openGallary(picker : UIImagePickerController)
    {
        //  picker.allowsEditing = true
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func openCamera(picker : UIImagePickerController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            //  picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            
            present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title:"" , message: "Device not support to take photos", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
}

