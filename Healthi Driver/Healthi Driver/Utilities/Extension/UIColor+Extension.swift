//
//  UIColor+Extension.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var appThemeGreenColor: UIColor { return UIColor.init(displayP3Red: 102/255, green: 156/255, blue: 36/255, alpha: 1.0) }
    
    static var appThemeBlackColor: UIColor { return UIColor.init(displayP3Red: 38/255, green: 38/255, blue: 40/255, alpha: 1.0) }
    
    static var appThemeGrayColor: UIColor { return UIColor.init(displayP3Red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0) }
    
    static var appThemeYellowColor: UIColor { return UIColor.init(displayP3Red: 240/255, green: 190/255, blue: 0/255, alpha: 1.0) }
    
    static var appThemeBlueColor: UIColor { return UIColor.init(displayP3Red: 91/255, green: 142/255, blue: 230/255, alpha: 1.0) }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
   

