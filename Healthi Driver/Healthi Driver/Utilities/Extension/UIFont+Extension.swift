//
//  UIFont+Extension.swift
//  Liber
//
//  Created by YASH on 22/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case medium = "SFUIDisplay-Medium"
    case heavy = "SFUIDisplay-Heavy"
    case light = "SFUIDisplay-Light"
    case bold = "SFUIDisplay-Bold"
}

extension UIFont
{

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
