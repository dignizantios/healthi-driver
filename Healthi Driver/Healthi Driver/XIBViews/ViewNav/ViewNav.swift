//
//  ViewNav.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit

class ViewNav: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- View life Cycle
    
    override func awakeFromNib() {
       
        
        if !isEnglish
        {
            lblTitle.textAlignment = .right
        }
        
        lblTitle.textColor = UIColor.white
        lblTitle.font = themeFont(size: 30, fontname: .medium)
        
    }

}
