//
//  LoginVc.swift
//  Healthi Driver
//
//  Created by Haresh on 27/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class LoginVc: UIViewController {

    //MARK: - Outlet
    
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblHealthiDriver: UILabel!
    @IBOutlet weak var lblSignInToContinue: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnSignIn: UIButton!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        
        appdelgate.objCustomTabBar = TabbarVc()
        
        self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
    }
    
}

extension LoginVc
{
    
    func setupUI()
    {
        
        lblHealthiDriver.font = themeFont(size: 35, fontname: .medium)
        lblHealthiDriver.textColor = UIColor.white
        lblHealthiDriver.text = getCommonString(key: "Healthi_driver_key")
        
        lblSignInToContinue.font = themeFont(size: 17, fontname: .light)
        lblSignInToContinue.textColor = UIColor.white
        lblSignInToContinue.text = getCommonString(key: "Sign_in_to_continue_key")
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        [txtEmail,txtPassword].forEach { (txt) in
            
            txt?.textColor = .white
            txt?.font = themeFont(size: 14, fontname: .light)
            txt?.delegate = self
            txt?.placeHolderColor = UIColor.white
            
        }
        
        btnSignIn.setupThemeButtonUI()
        btnSignIn.setTitle(getCommonString(key: "SIGN_IN_key"), for: .normal)
        
        if !isEnglish
        {
            //vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblHealthiDriver.textAlignment = .right
            lblSignInToContinue.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPassword.textAlignment = .right
            
        }
        
    }
    
}




extension LoginVc : UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
