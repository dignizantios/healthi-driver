//
//  ChangeLanguageVc.swift
//  Healthi Driver
//
//  Created by Haresh on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class ChangeLanguageVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var lblArabic: UILabel!
    
    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var imgArabic: UIImageView!
    
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Language_key"))
    }
    
    @IBAction func btnEnglishTapped(_ sender: UIButton) {
        
        if !isEnglish
        {
            setLangauge(language: English)
            resetupOfTabBar()
        }
        
    }
    
    @IBAction func btnArabicTapped(_ sender: UIButton) {
        
        if isEnglish
        {
            setLangauge(language: Arabic)
            resetupOfTabBar()
        }
    }
    
    func resetupOfTabBar()
    {
        appdelgate.objCustomTabBar = TabbarVc()
        appdelgate.window?.rootViewController = appdelgate.objCustomTabBar
    }
    
}

//MARK: - SetupUI

extension ChangeLanguageVc
{
    
    func setupUI()
    {
        
        [lblEnglish,lblArabic].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .light)
        }
        
        lblEnglish.text = getCommonString(key: "English_key")
        lblArabic.text = getCommonString(key: "Arabic_key")
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblArabic.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblEnglish.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblEnglish.textAlignment = .right
            lblArabic.textAlignment = .right
            imgArabic.isHidden = false
            imgEnglish.isHidden = true
            
            
        }
        else
        {
            imgArabic.isHidden = true
            imgEnglish.isHidden = false
        }
        
        
    }
}
