//
//  TabbarVc.swift
//  Healthi Driver
//
//  Created by Haresh on 27/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TabbarVc: UITabBarController,UITabBarControllerDelegate {

    //MARK: - Outlet
    
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        let orderObj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OrderVc") as! OrderVc
        let tabbarOneItem = UITabBarItem(title: getCommonString(key: "Orders_key"), image: UIImage(named:"ic_list_gray")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named:"ic_list_green"))
        orderObj.tabBarItem = tabbarOneItem
        
        let profileObj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
        
        let tabbarTwoItem = UITabBarItem(title: getCommonString(key: "Profile_key"), image: UIImage(named:"ic_profile_gray"), selectedImage: UIImage(named:"ic_profile_green"))
        profileObj.tabBarItem = tabbarTwoItem
        
        let n1 = UINavigationController(rootViewController:orderObj)
        let n2 = UINavigationController(rootViewController:profileObj)
        
//        n1.navigationBar.isHidden = false
//        n2.navigationBar.isHidden = false
        
        /*[n1,n2].forEach { (navigationBar) in
            navigationBar.navigationBar.isTranslucent = true
            
            
            navigationBar.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
            navigationBar.navigationBar.isHidden = false
            
            navigationBar.navigationItem.hidesBackButton = true
            
            navigationBar.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.navigationBar.shadowImage = UIImage()
            navigationBar.navigationBar.isTranslucent = true
            navigationBar.view.backgroundColor = .clear
            
        }
        */
        
        if isEnglish
        {
            self.viewControllers = [n1,n2]
            self.selectedIndex = 0
        }
        else
        {
            self.viewControllers = [n2,n1]
            self.selectedIndex = 1
        }
        
//        
//        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 3)
//        self.tabBar.layer.shadowRadius = 2
//        self.tabBar.layer.shadowColor = UIColor.black.cgColor
//        self.tabBar.layer.shadowOpacity = 0.3
        
        
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.appThemeGreenColor
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.appThemeGrayColor
        } else {
            // Fallback on earlier versions
        }
    }
    
}
