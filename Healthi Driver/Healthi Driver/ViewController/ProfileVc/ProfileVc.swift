//
//  ProfileVc.swift
//  Healthi Driver
//
//  Created by Haresh on 27/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class ProfileVc: UIViewController {

    //MARK: - Outlet
    
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblOrderHistory: UILabel!
    @IBOutlet weak var lblAddOrRemoveAddress: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblTermsOfUse: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    
    @IBOutlet weak var btnLogout: UIButton!
    
    //MARK: - Varaible
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(title: getCommonString(key: "Profile_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeNavigationCustomView()
    }
}

//MARK: - Seetup UI
extension ProfileVc
{
    
    func setupUI()
    {
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        lblName.textColor = UIColor.black
        lblName.font = themeFont(size: 16, fontname: .light)
        
        lblEmail.textColor = UIColor.appThemeGrayColor
        lblEmail.font = themeFont(size: 16, fontname: .light)
        
        [lblOrderHistory,lblNotification,lblLanguage,lblTermsOfUse,lblPrivacyPolicy].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 16, fontname: .light)
        }
        
        lblAddOrRemoveAddress.textColor = UIColor.appThemeGrayColor
        lblAddOrRemoveAddress.font = themeFont(size: 15, fontname: .light)
        
        lblOrderHistory.text = getCommonString(key: "Order_history_key")
        lblAddOrRemoveAddress.text = getCommonString(key: "Add_or_remove_a_delivery_address_key")
        
        lblNotification.text = getCommonString(key: "Notification_key")
        lblLanguage.text = getCommonString(key: "Language_key")
        lblTermsOfUse.text = getCommonString(key: "Terms_of_use_key")
        lblPrivacyPolicy.text = getCommonString(key: "Privacy_policy_key")
        
        btnLogout.setTitle(getCommonString(key: "Logout_key"), for: .normal)
        btnLogout.setTitleColor(UIColor.red, for: .normal)
        
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblName,lblEmail,lblOrderHistory,lblAddOrRemoveAddress,lblNotification,lblLanguage,lblPrivacyPolicy,lblTermsOfUse].forEach { (lbl) in
                
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            imgProfile.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnLogout.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
}

//MARK: - IBAction

extension ProfileVc
{
    
    @IBAction func btnOrderHistoryTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OrderHistoryListVc") as! OrderHistoryListVc
        
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
    }
    
    
    @IBAction func btnLanguageTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ChangeLanguageVc") as! ChangeLanguageVc
        
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnTermsOfUseTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnPrivacyTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
           self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func logoutAPICalling()
    {
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        
        let rearNavigation = UINavigationController(rootViewController: vc)
        appdelgate.window?.rootViewController = rearNavigation
    }
    
    
}
