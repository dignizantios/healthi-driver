//
//  OrderDetailTblCell.swift
//  Healthi Driver
//
//  Created by Haresh on 02/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class OrderDetailTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblExtraItemsAdded: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgItem.layer.cornerRadius = 6
        imgItem.layer.masksToBounds = true
        
        lblItemName.textColor = UIColor.appThemeBlackColor
        lblItemName.font = themeFont(size: 16, fontname: .light)
        
        lblExtraItemsAdded.textColor = UIColor.appThemeGrayColor
        lblExtraItemsAdded.font = themeFont(size: 14, fontname: .light)
        
        lblPrice.textColor = UIColor.appThemeGreenColor
        lblPrice.font = themeFont(size: 15, fontname: .light)
        
        if !isEnglish
        {
           // vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblItemName,lblExtraItemsAdded,lblPrice].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            imgItem.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
