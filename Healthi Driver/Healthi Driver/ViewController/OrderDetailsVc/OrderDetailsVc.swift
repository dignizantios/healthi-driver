//
//  OrderDetailsVc
//  Healthi Driver
//
//  Created by Haresh on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit


enum CheckParentControllerOfOrderDetails
{
    case fromOrder
    case fromOrderHistory
}


class OrderDetailsVc: UIViewController {

    //MARK: - Outlet
    
    
    
    @IBOutlet weak var constantHeightOfImage: NSLayoutConstraint!
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblOrderTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalPrice: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblDeliveryPrice: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    @IBOutlet weak var constantHeightOrderList: NSLayoutConstraint!
    @IBOutlet weak var tblOrderList: UITableView!
    
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblOrderValue: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderDateValue: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var lblOrderTimeValue: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusValue: UILabel!
    @IBOutlet weak var lblPickupAddress: UILabel!
    @IBOutlet weak var lblPickupAddressValue: UILabel!
    @IBOutlet weak var btnPickupPinDetails: UIButton!
    @IBOutlet weak var lblDeliveryDetails: UILabel!
    @IBOutlet weak var lblDeliveryDetailsValue: UILabel!
    @IBOutlet weak var btnDeliveryPinDetails: UIButton!
    
    @IBOutlet weak var btnPickup: UIButton!
    @IBOutlet weak var btnDeliver: UIButton!
    
    @IBOutlet weak var constantBtnPickupWidth: NSLayoutConstraint!
    @IBOutlet weak var constantBtnDeliverWidth: NSLayoutConstraint!
    
    
    
    //MARK: - Variable
    
    var selectedController = CheckParentControllerOfOrderDetails.fromOrder
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        tblOrderList.register(UINib(nibName: "OrderDetailTblCell", bundle: nil), forCellReuseIdentifier: "OrderDetailTblCell")
        
        tblOrderList.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setNavigationBarTransparent()
        tblOrderList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblOrderList.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()

    }
    
    
    @IBAction func btnPickupPinDetailsTapped(_ sender: UIButton) {
    }
    
    
    @IBAction func btnDeliveryPinDetailsTaped(_ sender: UIButton) {
    }
    
    
    @IBAction func btnColseTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Setup UI
extension OrderDetailsVc
{
    
    func setupUI()
    {
        
        
        if selectedController == .fromOrder
        {
            [btnPickupPinDetails,btnDeliveryPinDetails].forEach { (btn) in
                btn?.isHidden = false
            }
            
            [btnPickup,btnDeliver].forEach { (btn) in
                
                btn?.setTitleColor(UIColor.white, for: .normal)
                btn?.layer.masksToBounds = true
                btn?.layer.cornerRadius = 3.0
                btn?.titleLabel?.font = themeFont(size: 13, fontname: .light)
                btn?.isHidden = false
            }
        }
        else if selectedController == .fromOrderHistory
        {
            [btnPickupPinDetails,btnDeliveryPinDetails].forEach { (btn) in
                btn?.isHidden = true
            }
            
            [constantBtnPickupWidth,constantBtnDeliverWidth].forEach { (cosntant) in
                cosntant?.constant = 0
            }
            
            [btnPickup,btnDeliver].forEach { (btn) in
               
                btn?.isHidden = true
            }
        }
        
        
            btnDeliver.backgroundColor = UIColor.appThemeGreenColor
            btnDeliver.setTitle(getCommonString(key: "Deliver_key"), for: .normal)
            btnPickup.backgroundColor = UIColor.appThemeBlueColor
            btnPickup.setTitle(getCommonString(key: "Pickup_key"), for: .normal)
        
        
        if UIScreen.main.bounds.height >= 812
        {
            self.constantHeightOfImage.constant = 240
        }
        else
        {
            self.constantHeightOfImage.constant = 200
        }
        
        
        lblOrderTitle.text = getCommonString(key: "Order_key")
        lblOrderTitle.font = themeFont(size: 30, fontname: .medium)
        
        btnClose.setTitle(getCommonString(key: "Close_key"), for: .normal)
        btnClose.setTitleColor(UIColor.white, for: .normal)
        btnClose.titleLabel?.font = themeFont(size: 17, fontname: .light)
        [lblSubTotal,lblDelivery,lblOrder,lblOrderDate,lblOrderTime,lblStatus,lblOrderValue,lblOrderDateValue,lblOrderTimeValue,lblStatusValue].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 17, fontname: .light)
        }
        
        
        lblTotal.textColor = UIColor.appThemeBlackColor
        lblTotal.font = themeFont(size: 18, fontname: .medium)
        
        lblTotalPrice.textColor = UIColor.appThemeBlackColor
        lblTotalPrice.font = themeFont(size: 20, fontname: .medium)
        
        [lblPickupAddress,lblDeliveryDetails].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .light)
        }
        
        [lblPickupAddressValue,lblDeliveryDetailsValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        
        
        lblSubTotal.text = getCommonString(key: "Subtotal_key")
        lblDelivery.text = getCommonString(key: "Delivery_key")
        lblTotal.text = getCommonString(key: "Total_key")
        
        lblOrder.text = getCommonString(key: "Order_key")+"#"
        lblOrderDate.text = getCommonString(key: "Ordered_date_key")
        lblOrderTime.text = getCommonString(key: "Ordered_time_key")
        lblStatus.text = getCommonString(key: "Status_key")
        
        lblPickupAddress.text = getCommonString(key: "Pickup_details_key")
        lblDeliveryDetails.text = getCommonString(key: "Delivery_details_key")
        
        lblDeliveryDetailsValue.text = "102, Gopinath socity, Katargam, Surat, Gujarat, India"
        lblPickupAddressValue.text = "102, Gopinath socity, Katargam, Surat, Gujarat, India"
        
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            [lblOrderTitle,lblSubTotal,lblDelivery,lblTotal,lblOrder,lblOrderDate,lblOrderTime,lblStatus,lblPickupAddressValue,lblPickupAddress,lblDeliveryDetailsValue,lblDeliveryDetails].forEach { (lbl) in
            
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            btnClose.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblSubTotalPrice,lblDeliveryPrice,lblTotalPrice,lblOrderValue,lblOrderDateValue,lblOrderTimeValue,lblStatusValue].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .left
            }
            
        }
        
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            self.constantHeightOrderList.constant = tblOrderList.contentSize.height
        }
    }
}

extension OrderDetailsVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /*
         if arrayOrders.count == 0
         {
         let lbl = UILabel()
         lbl.text = strErrorMessage
         lbl.textAlignment = NSTextAlignment.center
         lbl.textColor = UIColor.appThemeGrayColor
         lbl.center = tableView.center
         tableView.backgroundView = lbl
         
         return 0
         }
         
         tableView.backgroundView = nil
         return arrayOrders.count
         */
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTblCell") as! OrderDetailTblCell
        
        if indexPath.row == 1
        {
            tblOrderList.separatorStyle = .none
        }
        else
        {
            tblOrderList.separatorStyle = .singleLine
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
