//
//  OrderHistoryListVc.swift
//  Healthi Driver
//
//  Created by Haresh on 04/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class OrderHistoryListVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblHistoryList: UITableView!
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblHistoryList.register(UINib(nibName: "OrderTblCell", bundle: nil), forCellReuseIdentifier: "OrderTblCell")
        
        tblHistoryList.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Order_history_key"))

    }
}


//MARK: - UITableview Delegate
extension OrderHistoryListVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /*
         if arrayOrders.count == 0
         {
         let lbl = UILabel()
         lbl.text = strErrorMessage
         lbl.textAlignment = NSTextAlignment.center
         lbl.textColor = UIColor.appThemeGrayColor
         lbl.center = tableView.center
         tableView.backgroundView = lbl
         
         return 0
         }
         
         tableView.backgroundView = nil
         return arrayOrders.count
         */
        
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTblCell") as! OrderTblCell
        
        let str = NSAttributedString()
        
        cell.lblOrderID.attributedText = str.attributedString(string1: getCommonString(key: "Order_ID_key"), string2: ": #511511", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeGrayColor, font1:  themeFont(size: 15, fontname: .light), font2:  themeFont(size: 15, fontname: .light))
        cell.lblDate.attributedText = str.attributedString(string1: getCommonString(key: "Date_key"), string2: ": 24-02-2019", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeGrayColor, font1:  themeFont(size: 15, fontname: .light), font2:  themeFont(size: 15, fontname: .light))
        
        cell.btnStatus.layer.masksToBounds = true
        cell.btnStatus.layer.cornerRadius = cell.btnStatus.layer.bounds.height/2
        
        if indexPath.row % 2 == 0
        {
            cell.btnStatus.backgroundColor = UIColor(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 0.1)
            cell.btnStatus.setTitle(getCommonString(key: "Delivered_key"), for: .normal)
            cell.btnStatus.setTitleColor(UIColor.black, for: .normal)
            cell.btnStatus.layer.borderColor = UIColor.appThemeGrayColor.cgColor
            cell.btnStatus.layer.borderWidth = 0.5
        }
        else{
            cell.btnStatus.backgroundColor = UIColor(red: 212.0/255.0, green: 60.0/255.0, blue: 79.0/255.0, alpha: 1.0)
            cell.btnStatus.setTitle(getCommonString(key: "Canceled_key"), for: .normal)
            cell.btnStatus.setTitleColor(UIColor.white, for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsVc") as! OrderDetailsVc
        obj.selectedController = .fromOrderHistory
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
