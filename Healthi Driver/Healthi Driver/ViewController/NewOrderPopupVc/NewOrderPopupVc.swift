//
//  NewOrderPopupVc.swift
//  Healthi Driver
//
//  Created by Haresh on 28/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class NewOrderPopupVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblNewOrder: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCancel: CustomButton!
    @IBOutlet weak var btnAccept: CustomButton!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        vwMain.roundCornersofView(corners: [.topLeft,.topRight], radius: 15)
        vwMain.layoutIfNeeded()
        vwMain.layoutSubviews()
    }
    
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        appdelgate.window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnAcceptTapped(_ sender: UIButton) {
        appdelgate.window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
}


//MARK: - Setup UI
extension NewOrderPopupVc
{
    
    func setupUI()
    {
        
        lblNewOrder.text = getCommonString(key: "You_have_a_new_order_key")
        lblNewOrder.textColor = UIColor.appThemeBlackColor
        lblNewOrder.font = themeFont(size: 18, fontname: .medium)
        
        lblItemName.textColor = UIColor.appThemeBlackColor
        lblItemName.font = themeFont(size: 15, fontname: .light)
        lblItemName.text = "Burger"
        
          let result = NSMutableAttributedString()
        var str1 = NSAttributedString()
        str1 = str1.attributedString(string1: getCommonString(key: "From_key"), string2: " " + "Salamya", color1: UIColor.appThemeGrayColor, color2: UIColor.appThemeBlackColor, font1:  themeFont(size: 12, fontname: .light), font2:  themeFont(size: 14, fontname: .light)) ?? str1
        
        var str2 = NSAttributedString()
        str2 = str2.attributedString(string1: " " + getCommonString(key: "To_key"), string2: " " + "Salamya", color1: UIColor.appThemeGrayColor, color2: UIColor.appThemeBlackColor, font1:  themeFont(size: 12, fontname: .light), font2:  themeFont(size: 14, fontname: .light)) ?? str2
        
        result.append(str1)
        result.append(str2)
        
        lblAddress.attributedText = result
        
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        btnCancel.backgroundColor = UIColor.white
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.cornerRadius = 2
        btnCancel.borderWidth = 1
        btnCancel.borderColor = UIColor.appThemeGreenColor
        btnCancel.setTitleColor(UIColor.appThemeGreenColor, for: .normal)
        
        btnAccept.setupThemeButtonUI()
        btnAccept.setTitle(getCommonString(key: "Accept_key"), for: .normal)
        btnAccept.cornerRadius = 2
        
        if !isEnglish
        {
            
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblNewOrder,lblItemName,lblAddress].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            imgItem.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            btnAccept.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnCancel.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        
        
    }
}
