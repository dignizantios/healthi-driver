//
//  OrderTblCell.swift
//  Healthi Driver
//
//  Created by Haresh on 27/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class OrderTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgUser.layer.masksToBounds = true
        imgUser.layer.cornerRadius = 2.0
        
        [lblName,lblOrderID,lblDate].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        
        lblName.textColor = UIColor.appThemeBlackColor
        
        lblPrice.font = themeFont(size: 15, fontname: .light)
        lblPrice.textColor = UIColor.appThemeGreenColor
        
        btnStatus.layer.masksToBounds = true
        btnStatus.layer.cornerRadius = 3.0
        btnStatus.titleLabel?.font = themeFont(size: 13, fontname: .light)
        
        lblName.text = "Shaikh Chilly"
        lblOrderID.text = "OrderID:#DFAD51"
        lblDate.text = "Date:12-05:2019"
        lblPrice.text = "10.00 KD"
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            imgUser.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            btnStatus.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblPrice.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblName,lblDate,lblOrderID].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
        }
        
        
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
