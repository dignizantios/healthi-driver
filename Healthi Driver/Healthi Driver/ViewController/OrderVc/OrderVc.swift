
//
//  OrderVc.swift
//  Healthi Driver
//
//  Created by Haresh on 27/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class OrderVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblOrder: UITableView!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblOrder.register(UINib(nibName: "OrderTblCell", bundle: nil), forCellReuseIdentifier: "OrderTblCell")
        
        tblOrder.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbar(title: getCommonString(key: "My_Orders_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeNavigationCustomView()
    }
    
    @objc func redirect()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "NewOrderPopupVc") as! NewOrderPopupVc
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        
        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
    }

}


//MARK: - UITableview Delegate
extension OrderVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /*
        if arrayOrders.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeGrayColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        
        tableView.backgroundView = nil
        return arrayOrders.count
 */
        
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTblCell") as! OrderTblCell
        
        let str = NSAttributedString()
        
        cell.lblOrderID.attributedText = str.attributedString(string1: getCommonString(key: "Order_ID_key"), string2: ": #511511", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeGrayColor, font1:  themeFont(size: 15, fontname: .light), font2:  themeFont(size: 15, fontname: .light))
        cell.lblDate.attributedText = str.attributedString(string1: getCommonString(key: "Date_key"), string2: ": 24-02-2019", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeGrayColor, font1:  themeFont(size: 15, fontname: .light), font2:  themeFont(size: 15, fontname: .light))
        
        
        cell.btnStatus.addTarget(self, action: #selector(redirect), for: .touchUpInside)
        
        if indexPath.row % 2 == 0
        {
            cell.btnStatus.backgroundColor = UIColor.appThemeGreenColor
            cell.btnStatus.setTitle(getCommonString(key: "Deliver_key"), for: .normal)
            cell.btnStatus.setTitleColor(UIColor.white, for: .normal)
        }
        else{
            cell.btnStatus.backgroundColor = UIColor.appThemeBlueColor
            cell.btnStatus.setTitle(getCommonString(key: "Pickup_key"), for: .normal)
            cell.btnStatus.setTitleColor(UIColor.white, for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsVc") as! OrderDetailsVc
        obj.selectedController = .fromOrder
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}
